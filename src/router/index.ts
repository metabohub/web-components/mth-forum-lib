// Composables
import { createWebHashHistory, createRouter } from 'vue-router'

// in-house views
import About from "@/views/About.vue";
import HowToInstall from "@/views/HowToInstall.vue";
import Demo from "@/views/Demo.vue";

// tutos
import TutoFormFinder from "@/views/TutoFormFinder.vue";
import TutoResultsDisplay from "@/views/TutoResultsDisplay.vue";
import TutoLiteratureDisplay from "@/views/TutoLiteratureDisplay.vue";
import TutoDownloadFile from "@/views/TutoDownloadFile.vue";

// define routes
const routes = [
  {
    path: '/',
    redirect: '/how-to-install'
  },
  {
    path: "/about",
    name: "About",
    component: About,
  },
  {
    path: "/demo",
    name: "Demo",
    component: Demo,
  },
  {
    path: "/how-to-install",
    name: "How to Install",
    component: HowToInstall,
  },
  {
    path: "/tuto-form-finder",
    name: "Form Finder",
    component: TutoFormFinder,
  },
  {
    path: "/tuto-results-display",
    name: "Results Display",
    component: TutoResultsDisplay,
  },
  {
    path: "/tuto-download-file",
    name: "File Download",
    component: TutoDownloadFile,
  },
  {
    path: "/tuto-literature-display",
    name: "Literature Display",
    component: TutoLiteratureDisplay,
  },
];

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes,
})

export default router;
