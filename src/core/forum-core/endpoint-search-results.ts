import {
  EndpointSearchQuery,
  SearchEntityType,
  SelectorType,
} from "./endpoint-search-query";
import { ChebiApi, ChemontApi, PubchemApi } from "@/core/openapi-ts/api";

/**
 * Core class for Rest API queries
 */
export class EndpointSearchResults {
  searchQuery: EndpointSearchQuery; // Query descriptor
  searchResults: any[]; // Query results (array of Mesh)

  constructor(
    private chebi_service: ChebiApi,
    private chemont_service: ChemontApi,
    private pubchem_service: PubchemApi
  ) {
    // init constructor parameters
    this.chebi_service = new ChebiApi();
    this.chemont_service = new ChemontApi();
    this.pubchem_service = new PubchemApi();
    // init  class parameters
    this.searchQuery = new EndpointSearchQuery();
    this.searchResults = [];
  }

  /**
   * ASYNC: Function to get the data requested from the Rest API
   * Returns this object (with the results updated) in a Promise
   */
  getAPIdata(): Promise<any> {
    return new Promise((resolve, reject) => {
      switch (this.searchQuery.searchEntityType) {
        // Search by compound => specification of input table
        // /pubchem_mesh/{idPubchem}
        // /chemont_mesh/{idChemont}
        // /chebi_mesh/{idChebi}
        case SearchEntityType.comp_to_mesh: {
          // Specification of Input table
          switch (this.searchQuery.searchSelectorType) {
            // Request data w/ Pubchem ID
            case SelectorType.pubchem_id: {
              // const service = new PubchemApi();
              // service
              this.pubchem_service
                .pubchemMeshIdPubchemGet(+this.searchQuery.searchEntityQuery)
                // eslint-disable-next-line
                .then((value: any) => {
                  // const compoundData = value.data as Compound;
                  // console.log("compoundData ⇒ ", compoundData);
                  // this.compound = JSON.stringify(compoundData);
                  console.debug("[COMP_PUBCHEM] fetched objects : ", value);
                  this.searchResults = value;
                  resolve(this);
                })
                // eslint-disable-next-line
                .catch((error: any) => {
                  // this.compound = "{}";
                  console.error("pubchem response ⇒ ", error);
                  // TODO process error
                  // if (error.response.status == 401) {
                  //   this.cpdApiError = "401 error - API token is set but not valid";
                  // } else if (error.response.status == 500) {
                  //   this.cpdApiError = "500 error - API request is not valid";
                  // } else {
                  //   this.cpdApiError = error.response.status + " error - Unknown error";
                  // }
                });
              //   next(data) {
              //     console.debug('[COMP_PUBCHEM] fetched objects : ', data);
              //     // this.searchResults = data;
              //     resolve(this);
              //   },
              //   error(msg) {
              //     console.debug('[COMP_PUBCHEM] Error Getting Pubchem service : ', msg);
              //     reject(this);
              //   }
              // });
              break;
            }

            // Request data w/ Chemont ID
            case SelectorType.chemont_id: {
              this.chemont_service
                .chemontMeshIdChemontGet(this.searchQuery.searchEntityQuery)
                // eslint-disable-next-line
                .then((value: any) => {
                  // const compoundData = value.data as Compound;
                  // console.log("compoundData ⇒ ", compoundData);
                  // this.compound = JSON.stringify(compoundData);
                  console.debug("[COMP_CHEMONT] fetched objects : ", value);
                  this.searchResults = value;
                  resolve(this);
                })
                // eslint-disable-next-line
                .catch((error: any) => {
                  // this.compound = "{}";
                  console.error("chemont response ⇒ ", error);
                  // TODO process error
                });
              // this.chemont_service.chemontMeshIdChemontGet(this.searchQuery.searchEntityQuery).subscribe({
              //   next(data) {
              //     console.debug('[COMP_CHEMONT] fetched objects : ', data);
              //     // this.searchResults = data;
              //     resolve(this);
              //   },
              //   error(msg) {
              //     console.debug('[COMP_CHEMONT] Error Getting Chemont service : ', msg);
              //     reject(this);
              //   }
              // });
              break;
            }

            // Request data w/ Chebi ID
            case SelectorType.chebi_id: {
              this.chebi_service
              .chebiMeshIdChebiGet(this.searchQuery.searchEntityQuery)
              // eslint-disable-next-line
              .then((value: any) => {
                // const compoundData = value.data as Compound;
                // console.log("compoundData ⇒ ", compoundData);
                // this.compound = JSON.stringify(compoundData);
                console.debug("[COMP_CHEBI] fetched objects : ", value);
                this.searchResults = value;
                resolve(this);
              })
              // eslint-disable-next-line
              .catch((error: any) => {
                // this.compound = "{}";
                console.error("chebi response ⇒ ", error);
                // TODO process error
              });
              // this.chebi_service.chebiMeshIdChebiGet(this.searchQuery.searchEntityQuery).subscribe({ // + to transform string into int
              //   next(data) {
              //     console.debug('[COMP_CHEBI] fetched objects : ', data);
              //     // this.searchResults = data;
              //     resolve(this);
              //   },
              //   error(msg) {
              //     console.debug('[COMP_CHEBI] Error Getting Chebi service : ', msg);
              //     reject(this);
              //   }
              // });
              break;
            }

            // Request data w/ PubChem Fuzzy search
            case SelectorType.pubchem_fuzzy_id: {
              // console.log("[info] pubchem CID = ", this.searchQuery.searchEntityQuery);
              // this.pubchem_service.pubchemMeshIdPubchemGet(+this.searchQuery.searchEntityQuery).subscribe({
              //   next(data) {
              //     console.debug('[COMP_PUBCHEM] fetched objects : ', data);
              //     // this.searchResults = data;
              //     resolve(this);
              //   },
              //   error(msg) {
              //     console.debug('[COMP_PUBCHEM] Error Getting Pubchem service : ', msg);
              //     reject(this);
              //   }
              // });
              break;
            }
          }
          break;
        }

        // Search by MeSH => specification of output table
        // /mesh_pubchem/{idMesh}
        // /mesh_chemont/{idMesh}
        // /mesh_chebi/{idMesh}
        case SearchEntityType.mesh_to_comp: {
          // Specification of Output table
          switch (this.searchQuery.searchSelectorType) {
            // Request data for Pubchem
            case SelectorType.pubchem_id: {
              const service = new PubchemApi();
              service
                .meshPubchemIdMeshGet(this.searchQuery.searchEntityQuery)
                // eslint-disable-next-line
                .then((value: any) => {
                  // const compoundData = value.data as Compound;
                  // console.log("compoundData ⇒ ", compoundData);
                  // this.compound = JSON.stringify(compoundData);
                  console.debug("[COMP_PUBCHEM] fetched objects : ", value);
                  this.searchResults = value;
                  resolve(this);
                })
                // eslint-disable-next-line
                .catch((error: any) => {
                  // this.compound = "{}";
                  // console.error("compound response ⇒ ", error);
                  // if (error.response.status == 401) {
                  //   this.cpdApiError = "401 error - API token is set but not valid";
                  // } else if (error.response.status == 500) {
                  //   this.cpdApiError = "500 error - API request is not valid";
                  // } else {
                  //   this.cpdApiError = error.response.status + " error - Unknown error";
                  // }
                });

              // this.pubchem_service.meshPubchemIdMeshGet(this.searchQuery.searchEntityQuery).subscribe({
              //   next(data) {
              //     console.debug('[MESH_PUBCHEM] fetched objects : ', data);
              //     // this.searchResults = data;
              //     resolve(this);
              //   },
              //   error(msg) {
              //     console.debug('[MESH_PUBCHEM] Error Getting Pubchem service : ', msg);
              //     reject(this);
              //   }
              // });
              break;
            }

            // Request data for Chemont
            case SelectorType.chemont_id: {
              // this.chemont_service.meshChemontIdMeshGet(this.searchQuery.searchEntityQuery).subscribe({
              //   next(data) {
              //     console.debug('[MESH_CHEMONT] fetched objects : ', data);
              //     // this.searchResults = data;
              //     resolve(this);
              //   },
              //   error(msg) {
              //     console.debug('[MESH_CHEMONT] Error Getting Chemont service : ', msg);
              //     reject(this);
              //   }
              // });
              break;
            }

            // Request data for Chebi
            case SelectorType.chebi_id: {
              // this.chebi_service.meshChebiIdMeshGet(this.searchQuery.searchEntityQuery).subscribe({
              //   next(data) {
              //     console.debug('[MESH_CHEBI] fetched objects : ', data);
              //     // this.searchResults = data;
              //     resolve(this);
              //   },
              //   error(msg) {
              //     console.debug('[MESH_CHEBI] Error Getting Chebi service : ', msg);
              //     reject(this);
              //   }
              // });
              break;
            }
          }
          break;
        }

        // Error case
        default: {
          console.debug(
            "Error: fetching data from API (400 invalid request type)"
          );
          reject(this);
          break;
        }
      }
    });
  }
}
