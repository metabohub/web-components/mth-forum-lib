/**
 * Types of query available
 */
export enum SearchEntityType {
  comp_to_mesh = "comp_to_mesh", // From Compound to MeSH 
  mesh_to_comp = "mesh_to_comp"  // From MeSH to Compound
}

/**
 * Differents Input/Output available
 */
export enum SelectorType {
  mesh_id = "mesh_id",        // MeSH
  chebi_id = "chebi_id",      // Chebi
  chemont_id = "chemont_id",  // Chemont
  pubchem_id = "pubchem_id" ,  // Pubchem
  pubchem_fuzzy_id = "pubchem_fuzzy_id"   // Pubchem fuzzy
}

/**
 * Describes the query to the Rest API
 */
export class EndpointSearchQuery {

  searchEntityType: SearchEntityType;
  searchSelectorType: SelectorType;
  searchEntityQuery: string; // String passed via the search bar

  constructor() {
    this.searchEntityType = SearchEntityType.comp_to_mesh; // Type: From Compound to MeSH by default
    this.searchSelectorType = SelectorType.pubchem_id;     // I/O: Pubchem by default
    this.searchEntityQuery = "";                           // Query: empty by default
  }

  /**
   * Function to verify if the query is empty
   */
  isEmpty(): boolean {
    return (!this.searchEntityQuery || 0 === this.searchEntityQuery.length || !this.searchEntityQuery.trim());
  }
}
