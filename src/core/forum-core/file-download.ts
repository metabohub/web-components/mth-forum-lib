// ref. import
import moment from "moment";

// in-house import
import { SearchEntityType, SelectorType } from "./endpoint-search-query";
import { EndpointSearchResults } from "./endpoint-search-results";

/**
 * Launch the download file action
 * @param endpointSearchResults the endpoint configuration
 */
export function launchFileDownload(//
    endpointSearchResults: EndpointSearchResults,
): void {
    console.debug("[info] use calling 'launchFileDownload()' method");
    // init data to download
    const raw_res = endpointSearchResults?.searchResults;
    // check if data are consistent
    if (!(endpointSearchResults &&
        raw_res.length > 0)) {
        return void (0);
    }
    //init file name
    const mesh_suffix: string = "-MeSH";
    let filename: string;
    // generating the file name suffix
    const searchQuery = endpointSearchResults.searchQuery;
    if (searchQuery.searchEntityType === SearchEntityType.comp_to_mesh) {
        if (searchQuery.searchSelectorType === SelectorType.pubchem_id) {
            filename = "CID" + searchQuery.searchEntityQuery + mesh_suffix;
        } else {
            filename = searchQuery.searchEntityQuery + mesh_suffix;
        }
    } else {
        filename = searchQuery.searchEntityQuery + "-" + searchQuery.searchSelectorType.slice(0, -3);
    }
    // init CSV header
    const csvHeader = Object.keys(raw_res[0])
        .map((j) => `"${j}"`)
        .join(",");
    // init CSV content
    const csvBody = raw_res
        .map((j: any) =>
            Object.values(j) //
                .map((j) => (`${j}`).replace('"', '""')) // convert ot string / protect double quote in texts
                .map((j) => `"${j}"`) // add double quote before/after text
                .join(",") // values separator: comma
        )//
        .join("\n"); // set a new line
    // launch download in file
    downloadFile(`${csvHeader}\n${csvBody}`, filename);
}

/**
 * launch download file action
 * @param data the DATA to dump into the file to download
 * @param filename the NAME fo the file, once downloaded 
 */
function downloadFile(data: any, filename = "data"): void {
    const blob = new Blob(["\ufeff" + data], {
        type: "text/csv;charset=utf-8;",
    });
    const downloadLink = document.createElement("a");
    const url = URL.createObjectURL(blob);
    const isSafariBrowser =
        navigator.userAgent.indexOf("Safari") != -1 &&
        navigator.userAgent.indexOf("Chrome") == -1;
    // if Safari open in new window to save file with random filename.
    if (isSafariBrowser) {
        downloadLink.setAttribute("target", "_blank");
    }
    downloadLink.setAttribute("href", url);
    // get the current date to create the filename
    const now = moment(new Date()).format("YYYY-MM-DD_hh-mm");
    // finish to build download button (hidden), click on it, then remove it;
    downloadLink.setAttribute("download", now + "_FORUM-DC_" + filename + ".csv");
    downloadLink.style.visibility = "hidden";
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
}