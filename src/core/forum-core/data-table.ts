/**
 * Describes the display of a row
 */
export class DisplayResult {
  id: string; // ID of compound/concept
  name: string; // Name of compound/concept
  link: string; // Link to official page
  root: string; // Tree root of a MeSH element
  meshTree: string; // MeSH Tree-Numbers
  qvalue: number; // q.value of compound/concept
  oddratio: string; // odd ratio of compound/concept
  papers: number; // Number of article common between MeSH and Pubchem/Chemont/Chebi of compound/concept
  score: number; // Weakness of compound/concept (Nullable)

  constructor(
    private _id: string,
    private _name: string,
    private _link: string,
    private _root: string,
    private _meshTree: string,
    private _qvalue: number,
    private _oddratio: string,
    private _papers: number,
    private _score: number
  ) {
    this.id = _id;
    this.name = _name;
    this.link = _link;
    this.root = _root;
    this.meshTree = _meshTree;
    this.qvalue = _qvalue;
    this.oddratio = _oddratio;
    this.papers = _papers;
    this.score = _score;
  }
}
