export class EventDiscoveryService {
  name: string;
  value: any;

  constructor(_name: string, _value: any) {
    this.name = _name;
    this.value = _value;
  }
}
