export interface PMID {
  PMID: string;
  Title: string;
  Date: string;
  urlPMID: string;
}

export function emptyPMID(): PMID {
  return {
    PMID: "",
    Title: "",
    Date: "",
    urlPMID: "",
  };
}
