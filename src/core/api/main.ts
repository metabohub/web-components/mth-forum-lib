/**
 * Method to get HASH parameters (map of KEY/VALUES in a second HASH after the router one)
 * @returns method to get HASH parameters
 */
export function getHashParameters(): Map<String, String> {
  const params: Map<String, String> = new Map<String, String>();
  const pageHash = document.location.hash;
  if (pageHash.split("#").length === 3) {
    const rawHashParam = pageHash.split("#")[2];
    for (const i of rawHashParam.split("&").values()) {
      const j = i.split("=");
      if (j.length === 2) {
        params.set(j[0], j[1]);
      }
    }
  }
  return params;
}

export function setHashParameter(key: string, value: string): void {
  //get original hash
  const pageHash = document.location.hash;
  // get current params
  const params = getHashParameters();
  // set specifique param
  if (!(value === undefined || value === "" || value === null)) {
    params.set(key, value);
  }
  // or remove key
  else {
    params.delete(key);
  }
  // build HASH params as string
  let hashParam = "";
  params.forEach((v, k) => {
    hashParam += "&" + k + "=" + v;
  });
  document.location.hash = "#" + pageHash.split("#")[1] + "#" + hashParam;
}
