import type { App } from 'vue';
import {
    MthForumFormFinder,//
    MthForumLiteratureDisplay,//
    MthForumResultsDisplay,//
} from "@/components";

import { ChebiApi, ChemontApi, PubchemApi } from '@/core/openapi-ts';
import { EndpointSearchQuery } from "@/core/forum-core/endpoint-search-query";
import { EndpointSearchResults } from "@/core/forum-core/endpoint-search-results";

const MetabohubForumLibrary = {
    install: (app: App) => {
        // forum components
        app.component('MthForumFormFinder', MthForumFormFinder);
        app.component('MthForumLiteratureDisplay', MthForumLiteratureDisplay);
        app.component('MthForumResultsDisplay', MthForumResultsDisplay);
    },
};

export {
    // all components
    MetabohubForumLibrary,
    // TS code
    ChebiApi,//
    ChemontApi,//
    PubchemApi,//
    EndpointSearchQuery,//
    EndpointSearchResults,//
};