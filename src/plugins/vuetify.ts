/**
 * plugins/vuetify.ts
 *
 * Framework documentation: https://vuetifyjs.com`
 */

// Styles
import '@fortawesome/fontawesome-free/css/all.css' // Ensure your project is capable of handling css 
import 'vuetify/styles'

// Composables
import { createVuetify } from 'vuetify'

// Icons
import { aliases, fa } from 'vuetify/iconsets/fa'

// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
export default createVuetify({
  // tmp lab - use data-table
  components: {},
  // added
  ssr: true,
  // icons
  icons: {
    defaultSet: 'fa',
    aliases,
    sets: {
      fa,
    },
  },
  // default:
  theme: {
    defaultTheme: "light",
    themes: {
      light: {
        colors: {
          primary: '#1867C0',
          secondary: '#5CBBF6',
        },
      },
      dark: {
        colors: {
          primary: '#FB8C00',
          secondary: '#9E9EFF',
        },
      },
    },
  },
})
