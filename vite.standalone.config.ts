// Plugins
import vue from '@vitejs/plugin-vue'
import vuetify, { transformAssetUrls } from 'vite-plugin-vuetify'

// Utilities
import { defineConfig, loadEnv } from 'vite'
import path from 'path'

// https://vitejs.dev/config/ 
export default ({ mode }) => {

  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };

  // import.meta.env.VITE_NAME available here with: process.env.VITE_NAME
  // import.meta.env.VITE_PORT available here with: process.env.VITE_PORT

  return defineConfig({
    build: {
      chunkSizeWarningLimit: 50000,
      target: "es2020",
      cssCodeSplit: true,
      rollupOptions: {
        external: ['emitter'],
        output: {
          manualChunks(id) {
            if (id.includes('node_modules')) {
              return id.toString().split('node_modules/')[1].split('/')[0].toString();
            }
          }
        }
      },
    },
    optimizeDeps: {
      force: true,
      // include: ['esm-dep > cjs-dep'],
      esbuildOptions: {
        target: "es2020",
        supported: {
          bigint: true
        }
      },
    },
    plugins: [
      vue({
        template: {
          transformAssetUrls,
          compilerOptions: { whitespace: 'preserve' },
        }
      }),
      // https://github.com/vuetifyjs/vuetify-loader/tree/next/packages/vite-plugin
      vuetify({
        autoImport: true,
        styles: {
          configFile: 'src/styles/settings.scss',
        },
      }),
      // createVuePlugin({
      //   vueTemplateOptions: {
      //     compilerOptions: {
      //       whitespace: 'preserve'
      //     }
      //   }
      // }),
    ],
    define: {
      'process.env': process.env
    },
    // resolve: {
    //   alias: {
    //     '@': path.resolve(__dirname, 'src'),
    //   },
    //   extensions: [
    //     '.js',
    //     '.json',
    //     '.jsx',
    //     '.mjs',
    //     '.ts',
    //     '.tsx',
    //     '.vue',
    //   ],
    // },
    server: {
      port: 3333,
    },
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src'),
        // emitter: require.resolve('emitter-component'),
      },
    },
  });
}
