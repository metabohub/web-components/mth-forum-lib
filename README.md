# MetaboHUB - Forum Library

[![pipeline status](https://forgemia.inra.fr/metabohub/web-components/mth-forum-lib/badges/dev/pipeline.svg)](https://forgemia.inra.fr/metabohub/web-components/mth-forum-lib/-/commits/dev)
[![coverage report](https://forgemia.inra.fr/metabohub/web-components/mth-forum-lib/badges/dev/coverage.svg)](https://forgemia.inra.fr/metabohub/web-components/mth-forum-lib/-/commits/dev)
[![Latest Release](https://forgemia.inra.fr/metabohub/web-components/mth-forum-lib/-/badges/release.svg)](https://forgemia.inra.fr/metabohub/web-components/mth-forum-lib/-/releases)

## Metadata

* authors: <nils.paulhe@inrae.fr>, <olivier.filangi@inrae.fr>
* creation date: `2023-01-20`
* main usage: Vue.js webcomponent use for [Forum Web-Applications](https://forum-webapp.semantic-metabolomics.fr/).

FORUM is an open knowledge network aiming at supporting metabolomics results interpretation in biomedical sciences and automated reasoning.
Containing more than 8 billion statements, it federate data from several life-science resources such as PubMed, ChEBI and PubChem.
Leveraging the bridges in this linked dataset, we automatically extract associations between compound and biomedical concepts, using literature metadata enrichment.

## Getting Started

This WebComponent library is compatible with projects using [Vue.js 3](https://vuejs.org/) and [TypeScript](https://www.typescriptlang.org/).
This project is based on [Vue.js](https://vuejs.org/), [Vite](https://vitejs.dev/guide/) and [Vuetify](https://vuetifyjs.com/en/) frameworks.

### Installing

Note: this section is for developers who want to implement and use this library into their own project(s).
To add the library to your [Node.js](https://nodejs.org/) project, please enter the following commands: `npm i @metabohub/forum-library`

Then you must import the library in your `src/main.ts` file:

```js
// import the library and its styles
import { MetabohubForumLibrary } from '@metabohub/forum-client-library';
import '@metabohub/forum-client-library/dist/src/components/main.css';

const app = createApp(App);
registerPlugins(app); // <= import your plugin like vuetify, ...
app.use(MetabohubForumLibrary); // <= add this line!
app.mount('#app');
```

Finally you can add our WebComponent in you own Vue components:

```html
<!-- Template -->
<template>
  <div class="find-associations">
    <mth-forum-form-finder @search-query="onSearchQuery" />
    <mth-forum-results-display :search-results="dataSearchResults" />
  </div>
</template>
<!-- Code -->
<script lang="ts">
// from Vue core
import { ref } from "vue";
// forum - core services and mappers
import { //
  ChebiApi, ChemontApi, PubchemApi,//
  EndpointSearchQuery, EndpointSearchResults//
} from "@metabohub/forum-library";
// main
export default {
  name: "Demo",
  components: {},
  setup() {
    return {
      dataSearchResults: ref(new EndpointSearchResults(new ChebiApi(), new ChemontApi(), new PubchemApi()))
    };
  },
  methods: {
    onSearchQuery(data: EndpointSearchQuery): void {
      // Create new query results
      this.dataSearchResults = new EndpointSearchResults(new ChebiApi(), new ChemontApi(), new PubchemApi());
      // Apply the query emmited
      this.dataSearchResults.searchQuery = data;
    },
  },
};
</script>
<!-- Style -->
<style lang="scss">
form {
  fieldset {
    padding: 5px;

    legend {
      padding: 5px;
      margin-left: 20px;
    }
  }
}
</style>
```

<!-- TODO 
## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```bash
Give an example
```

### And coding style tests

Explain what these tests test and why

```bash
Give an example
```
-->

## Developments

### Prerequisites

Note: this section is for developers who want to get involved into this project.

* Node 16.15.0 (mandatory) - we recommand you to use [Node Version Manager](https://github.com/nvm-sh/nvm)
* Vue 3 (mandatory) - install with `npm install -g vue@next`
* git (recommended) - used during build to extract current commit hash/tag into showcase webapp footer.
* curl (very optional) - only used for CI/CD

### Edit Forum endpoint connection

If you edit `src/assets/doc_forum-openapi.json` file, you must re-generate the endpoint client code thanks `npm run openapi-generator` code.

### Library build and publish

Build and publish the library

```bash
# install node modules listed into 'package.json' / 'package-lock.json' files
npm install
# build the library
npm run build:lib
npm pack
# publish the library
npm publish
```

Notes:

* the publication registry is set into these files:
  * [.npmrc](.npmrc).
  * [package.json](package.json) - key "publishConfig".
* the publication is automatic during CI/CD process; see [.gitlab-ci.yml](.gitlab-ci.yml) file.
* during the GitLab CI/CD pipeline, the previous package with the same version identifier is removed thanks:
  * `curl` commands - to get all published packages in targeted GitLab packages registry and remove a specific one\
  (it require a GitLab API token with correct authorizations under `MASTER_TOKEN` CI/CD variable)

You can use non-stable version of the library using the GitLab npm package regisery:

```bash
# if you already have installed the library, you must remove the previous one:
npm remove @metabohub/forum-library
# config GitLab npm registry
npm config set -- //forgemia.inra.fr/api/v4/projects/11325/packages/npm/:_authToken=__YOUR_GITLAB_TOKEN_HERE__
npm config set @metabohub:registry https://forgemia.inra.fr/api/v4/projects/11325/packages/npm/
# to install the library in your current Node.js project:
npm i --save \
  --registry=https://forgemia.inra.fr/api/v4/projects/11325/packages/npm/ \
  @metabohub/forum-library;
```

### Standalone showcase build

Build the standalone version with these commands

```bash
# install node modules listed into 'package.json' / 'package-lock.json' files
npm install
# either build the DEV standalone version
npm run build:dev
# or build the PROD standalone version
npm run build:prod
```

Please refer to [.env](.env), [.env.developement](.env.developement) and [.env.production](.env.production) files to see changes between PROD and DEV versions.

Note: you can run the standalone version into "dev mode" thanks `npm run dev` command.
<!--
## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.
-->

## Versioning

We use [GitLab](https://forgemia.inra.fr/metabohub/web-components/mth-forum-lib/) for versioning.
For the versions available, see the [tags on this repository](https://forgemia.inra.fr/metabohub/web-components/mth-forum-lib/tags).

Consult the [Changelog file](CHANGELOG.md) for further informations about versions and changes.

## Authors

* **Nils Paulhe** - *Initial work* - MetaboHUB, INRAE, PFEM.
* **Olivier Filangi** - *Discovery* - MetaboHUB, INRAE, P2M2.

See also the list of [contributors](https://forgemia.inra.fr/metabohub/web-components/mth-forum-lib/-/graphs/dev) who participated in this project.

## License

This project is licensed under the `CeCILL-C` License - see the [LICENSE.txt](LICENSE.txt) file for details

## Acknowledgments

* [MetaboHUB](https://www.metabohub.fr/home.html)
* [INRAE](https://www.inrae.fr/en)
* [FORUM](https://forum-webapp.semantic-metabolomics.fr/)
