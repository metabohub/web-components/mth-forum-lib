// powered by https://stackoverflow.com/questions/42155115/how-to-include-git-revision-into-angular-cli-application

// This script runs operations *synchronously* which is normally not the best
// approach, but it keeps things simple, readable, and for now is good enough.

const { gitDescribeSync } = require('git-describe');
const { writeFileSync } = require('fs');
const execSync = require('child_process').execSync;

// infos about git commit
const gitInfo = gitDescribeSync();

// infos about current build date: current timestamp in milliseconds
const timestamp = Date.now();
gitInfo['timestamp'] = timestamp;

// infos about currenttag
const output = execSync('git describe --tags --always || echo "no-tag"', { encoding: 'utf-8' });
gitInfo['tag'] = output.trim();

// write in file
const versionInfoJson = JSON.stringify(gitInfo, null, 2);
writeFileSync('./src/assets/git-version.json', versionInfoJson);
