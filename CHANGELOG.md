# Change Log | `MetaboHUB - Forum Library`

## About

All notable changes to `MetaboHUB - Forum Library` project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased] - 2023-02-07

Initial release of the library.

### Added

- [forum#dev]
  MAJOR - add basic web-components: `<mth-forum-form-finder />`, `<mth-forum-results-display />` and `<mth-forum-literature-display />`
