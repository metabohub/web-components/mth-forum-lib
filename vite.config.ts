// Plugins
import vue from '@vitejs/plugin-vue'
import vuetify, { transformAssetUrls } from 'vite-plugin-vuetify'

// Utilities
import { defineConfig, loadEnv } from 'vite'
import path from 'path'

// bluid as library
import typescript2 from 'rollup-plugin-typescript2';
import dts from "vite-plugin-dts";

// https://vitejs.dev/config/ 
export default ({ mode }) => {

  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };

  // import.meta.env.VITE_NAME available here with: process.env.VITE_NAME
  // import.meta.env.VITE_PORT available here with: process.env.VITE_PORT

  return defineConfig({
    plugins: [
      vue({
        template: { transformAssetUrls }
      }),
      dts({
        insertTypesEntry: true,
      }),
      typescript2({
        check: false,
        include: [
          "src/components/**/*.vue"
        ],
        tsconfigOverride: {
          compilerOptions: {
            outDir: "dist",
            sourceMap: true,
            declaration: true,
            declarationMap: true,
          },
        },
        exclude: ["vite.config.ts"]
      }),
      // https://github.com/vuetifyjs/vuetify-loader/tree/next/packages/vite-plugin
      vuetify({
        autoImport: true,
        styles: {
          configFile: 'src/styles/settings.scss',
        },
      }),
    ],
    define: { 'process.env': process.env },
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src'),
      },
      extensions: [
        '.js',
        '.json',
        '.jsx',
        '.mjs',
        '.ts',
        '.tsx',
        '.vue',
      ],
    },
    server: {
      port: 3000,
    },
    optimizeDeps: {
      force: true,
      include: ['esm-dep > cjs-dep'],
      esbuildOptions: {
        target: "es2020",
        supported: {
          bigint: true
        }
      }
    },
    build: {
      target: 'es2020',
      cssCodeSplit: true,
      lib: {
        // Could also be a dictionary or array of multiple entry points
        entry: "src/components/main.ts",
        name: 'MetabohubForumLibrary',
        formats: ["es", "cjs", "umd"],
        fileName: format => `mth-forum-lib-vue-ts.${format}.js`,
      },

      rollupOptions: {
        cache: false,
        // make sure to externalize deps that should not be bundled
        // into your library
        input: {
          main: path.resolve(__dirname, "src/components/main.ts")
        },
        external: ['vue'],
        output: {
          assetFileNames: (assetInfo) => {
            if (assetInfo.name === 'main.css') return 'mth-forum-lib-vue-ts.css';
            return assetInfo.name;
          },
          exports: "named",
          globals: {
            vue: 'Vue',
          },
        },
      },
    },
    // resolve: {
    //   alias: {
    //     '@': path.resolve(__dirname, 'src'),
    //   },
    // },
  });
}
